(function (exports) {
    'use strict';
    var strlpad = function(str, pad, len) {
        while (str.length < len) {
            str = pad + str;
        }
        return str;
    }

    var strrpad = function(str, pad, len) {
        while (str.length < len) {
            str = str + pad;
        }
        return str;
    }

    var strpart = function(str, n) {
            if (n > 0) {
                return str.match(new RegExp(".{1," + n + "}","g"));
            } else if (n < 0) {
                var an = Math.abs(n), t1 = (Math.ceil(str.length / an) * an) - str.length, t2 = "";
                for (var i = 0; i < t1; i++) {
                    t2 += "_";
                }
                t2 += str;
                t2 = t2.match(new RegExp(".{1," + an + "}","g"));
                t2[0] = t2[0].substr(t1, an - t1);
                return t2;
            }
    }

    var str2bin = function(str) {
        var output = "";
        for (var i = 0; i < str.length; i++) {
            output += strlpad(str.charCodeAt(i).toString(2), "0", 8);
        }
        return output;
    }

    var bin2str = function(bin) {
        var s = strpart(bin, 8), output = "";
        for (var i = 0; i < s.length; i++) {
            output += String.fromCharCode(parseInt(s[i], 2));
        }
        return output;
    }


    var str2hex = function(str) {
        var output = "";
        for (var i = 0; i < str.length; i++) {
            output += strlpad(str.charCodeAt(i).toString(16), "0", 2).toUpperCase();
        }
        return output;
    }

    var hex2str = function(hex) {
        var s = strpart(hex, 2), output = "";
        for (var i = 0; i < s.length; i++) {
            output += String.fromCharCode(parseInt(s[i], 16));
        }
        return output;
    }

    exports.stringConvert = exports.stringConvert || {
        strlpad: strlpad,
        strrpad: strrpad,
        str2bin: str2bin,
        bin2str: bin2str,
        str2hex: str2hex,
        hex2str: hex2str
    };
}(this));
