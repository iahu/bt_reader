;(function (exports) {
    'use strict';
    var labels = document.querySelectorAll('.label');
    var fileInput = document.querySelector('#f');
    var wrap = document.querySelector('.wrap');
    var divIn = document.querySelector('#in');
    var len = labels.length;
    var step = 2*Math.PI/len;
    var offsetLeft = wrap.offsetLeft;
    var offsetTop = wrap.offsetTop;
    var W = divIn.clientWidth;
    var H = divIn.clientHeight;
    var R = 120;
    var a, c;
    exports.tran = function () {
        for (var i = 0; i < len; i++) {
            a = [Math.cos(step*i)*R, Math.sin(step*i)*R];
            c = randColor();
            labels[i].style.left = a[0] + 'px';
            labels[i].style.top = a[1] + 'px';
            labels[i].style.background = 'rgb(' + c + ')';
            labels[i].style.color = 'rgb(' + reverseColor( c ) + ')';
        }
    }
    function randColor() {
        return [
            ~~Math.min(255, (Math.random() * 0xff + 30 )),
            ~~Math.min(255, (Math.random() * 0xff + 30 )),
            ~~Math.min(255, (Math.random() * 0xff + 30 )),
        ];
    }

    function reverseColor(c) {
        c[0] = 255 - c[0];
        c[1] = 255 - c[1];
        c[2] = 255 - c[2];
        return c;
    }
    function reverseValue(v) {
        v = (0xff - ('0x'+v) ).toString(16);
        return v === 0? '00' : ''+ v;
    }

    var dragStart = 0;
    addEvent(document, 'mousedown touchstart', onDragStart);
    addEvent(document, 'mouseup touchend touchcancel', onDragEnd);
    addEvent(document, 'mousemove touchmove', onDraging);

    function addEvent(el, name, handle) {
        name.split(' ').forEach(function(en) {
            el.addEventListener(en, handle);
        });
    }
    function onDragStart(e) {
        if ( e.target === divIn ) {
            dragStart = 1;
            divIn.classList.remove('dragend');
            divIn.classList.add('draging');
        }
    }
    function onDragEnd(e) {
        dragStart = 0;
        if (e.target === divIn || divIn.classList.contains('draging')) {
            divIn.classList.remove('draging');
            divIn.classList.add('dragend');
            divIn.style.left = 0;
            divIn.style.top = 0;
        }
    }
    function onDraging(e) {
        e.preventDefault();
        var px,py;
        if ( dragStart ) {
            if ( e.type === 'touchmove' ) {
                px = e.touches[0].pageX;
                py = e.touches[0].pageY;
            } else {
                px = e.pageX;
                py = e.pageY;
            }
            divIn.style.left = px - offsetLeft - W/2 + 'px';
            divIn.style.top = py -offsetTop - H/2 + 'px';
            tryOverlay(e);
        }
    }
    function tryOverlay(e) {
        var el = e.target;
        var l = el.offsetLeft;
        var t = el.offsetTop;
        var _l, _t, label;
        for (var i = 0; i < len; i++) {
            label = labels[i];
            _l = label.offsetLeft;
            _t = label.offsetTop;
            if ( (l-_l)*(l-_l) + (t-_t)*(t-_t) <= W*W ) {
                if ( !label.overlay ) {
                    exports.onOverlay( label );
                    label.overlay = true;
                }
            } else {
                label.overlay = false;
            }
        };
    }
}(this));
