(function (exports) {
    'use strict';
    var $file = document.querySelector('#f');
    var $divIn = document.querySelector('#in');
    var btData;

    $file.addEventListener('change', function(e) {
        var files = e.target.files;
        var reader, file;
        for (var i = 0; i < files.length; i++) {
            if (file = files[0]) {
                reader = new FileReader();
                reader.readAsBinaryString(file);
                if ( /.+\.torrent$/g.test(file.name) ) {
                    // iso-8859-1
                    // putLog(file.name);
                    reader.onload = loaded;
                }
            }
        }
    });
    function loaded(evt) {
        var res = evt.target.result;
        var arr = evt.target.result.split('')
        btData = Bencode.parse(res);

        try {
            // convertBt(obj);
            $file.style.display = 'none';
            $file.parentNode.classList.add('hasBT');
            exports.tran();
        } catch(e) {
            throw Error(e);
        }
    }

    function convertBt(input) {
        var type = typeof input;
        var rt = input;
        var convert = convertBt;
        switch (type) {
            case 'string':
                if ( + input == input ) {
                    rt = input;
                } else {
                    rt = utf82rstr(input);
                }
                break;
            case 'array':
                for (var i = 0; i < input.length; i++) {
                    rt.push( convert(input[i]) );
                }
                break;
            case 'object':
                for (var prop in input) {
                    if (input.hasOwnProperty(prop)) {
                        switch (prop) {
                            case 'pieces':
                            case 'ed2k':
                            case 'filehash':
                                rt[prop] = '0x' + stringConvert.str2hex( input[prop] );
                                break;
                            default:
                                rt[prop] = convert(input[prop]);
                        }
                    }
                }
                break;
        }
        return rt;
    }

    exports.onOverlay = function (el) {
        var d = replaceDic[el.dataset['rid']];
        var dict = d[~~(Math.random() * 100) % d.length];
        var info = btData.info;
        $divIn.style.background = el.style.background;
        $divIn.style.color = el.style.color;

        info['name.utf-8'] = rstr2utf8(dict.title);
        console.log(btData);
    }
}(this));